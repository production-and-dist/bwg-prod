<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.html$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule . /index.html [L]
  
  RewriteCond %{SERVER_PORT} !^443$
  RewriteRule .* https://%{SERVER_NAME}%{REQUEST_URI} [R=301,L]

  RewriteCond %{REQUEST_URI} !\?
  RewriteCond %{REQUEST_URI} !\&
  RewriteCond %{REQUEST_URI} !\=
  RewriteCond %{REQUEST_URI} !\.
  RewriteCond %{REQUEST_URI} !\/$
  RewriteRule ^(.*[^\/])$ /$1/ [R=301,L]

</IfModule>

<IfModule mod_headers.c>
  <FilesMatch "\.(ico|jpe?g|png|gif|swf)$">
    Header set Cache-Control "max-age=2592000, public"
  </FilesMatch>
  <FilesMatch "\.(css)$">
    Header set Cache-Control "max-age=604800, public"
  </FilesMatch>
  <FilesMatch "\.(js)$">
    Header set Cache-Control "max-age=216000, private"
  </FilesMatch>
  <FilesMatch "\.(x?html?|php)$">
    Header set Cache-Control "max-age=3600, private, must-revalidate"
  </FilesMatch>
  <filesMatch "\.(ico|pdf|flv|jpg|jpeg|png|gif|js|css|swf|svg)$">
    Header unset ETag
    FileETag None
  </filesMatch>
</IfModule>
<IfModule mod_expires.c>
  ExpiresActive on
  ExpiresDefault                                      "access plus 1 month"

  ExpiresByType text/css                              "access plus  0 seconds"

  ExpiresByType application/atom+xml                  "access plus 1 hour"
  ExpiresByType application/rdf+xml                   "access plus 1 hour"
  ExpiresByType application/rss+xml                   "access plus 1 hour"

  ExpiresByType application/json                      "access plus 0 seconds"
  ExpiresByType application/ld+json                   "access plus 0 seconds"
  ExpiresByType application/schema+json               "access plus 0 seconds"
  ExpiresByType application/vnd.geo+json              "access plus 0 seconds"
  ExpiresByType application/xml                       "access plus 0 seconds"
  ExpiresByType text/xml                              "access plus 0 seconds"

  ExpiresByType image/vnd.microsoft.icon              "access plus 1 week"
  ExpiresByType image/x-icon                          "access plus 1 week"

  ExpiresByType text/html                             "access plus 0 seconds"

  ExpiresByType application/javascript                "access plus 0 seconds"
  ExpiresByType application/x-javascript              "access plus 0 seconds"
  ExpiresByType text/javascript                       "access plus 0 seconds"

  ExpiresByType application/manifest+json             "access plus 1 year"

  ExpiresByType application/x-web-app-manifest+json   "access plus 0 seconds"
  ExpiresByType text/cache-manifest                   "access plus 0 seconds"

  ExpiresByType audio/ogg                             "access plus 1 month"
  ExpiresByType image/bmp                             "access plus 1 month"
  ExpiresByType image/gif                             "access plus 1 month"
  ExpiresByType image/jpeg                            "access plus 1 month"
  ExpiresByType image/png                             "access plus 1 month"
  ExpiresByType image/svg+xml                         "access plus 1 month"
  ExpiresByType image/webp                            "access plus 1 month"
  ExpiresByType video/mp4                             "access plus 1 month"
  ExpiresByType video/ogg                             "access plus 1 month"
  ExpiresByType video/webm                            "access plus 1 month"

  ExpiresByType application/vnd.ms-fontobject         "access plus 1 month"
  ExpiresByType font/eot                              "access plus 1 month"

  ExpiresByType font/opentype                         "access plus 1 month"

  ExpiresByType application/x-font-ttf                "access plus 1 month"

  ExpiresByType application/font-woff                 "access plus 1 month"
  ExpiresByType application/x-font-woff               "access plus 1 month"
  ExpiresByType font/woff                             "access plus 1 month"

  ExpiresByType application/font-woff2                "access plus 1 month"

  ExpiresByType text/x-cross-domain-polic             "access plus 1 week"
</IfModule>